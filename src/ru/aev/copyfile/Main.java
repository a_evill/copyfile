package ru.aev.copyfile;

import java.io.*;
import java.util.Scanner;

public class Main {
    private static final String INPUT_TEXT = "Введите путь до файла из которого нужно скопировать текст";
    private static final String OUTPUT_TEXT = "Введите путь файла в который нужно скопировать текст";

    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws InterruptedException {

        System.out.println(INPUT_TEXT);
        String onePathInputFile = getPathFile();
        System.out.println(OUTPUT_TEXT);
        String onePathOutputFile = getPathFile();

        System.out.println("\n" + INPUT_TEXT);
        String twoPathInputFile = getPathFile();
        System.out.println(OUTPUT_TEXT);
        String twoPathOutputFile = getPathFile();

        Thread oneFlow = new Thread(() -> getText(onePathInputFile, onePathOutputFile));
        Thread twoFlow = new Thread(() -> getText(twoPathInputFile, twoPathOutputFile));
        int sumParallel = parallelOfCopy(oneFlow,twoFlow);
        int sumSequentil = sequentialOfCopy(oneFlow,twoFlow);
        System.out.println(sumParallel + " " + sumSequentil);

    }
    private static int parallelOfCopy(Thread oneFlow, Thread twoFlow) throws InterruptedException {
        final long before = System.currentTimeMillis();
        oneFlow.start();
        twoFlow.start();
        oneFlow.join();
        twoFlow.join();
        final long after = System.currentTimeMillis();

        return (int) (after-before);
    }

    private static int sequentialOfCopy(Thread oneFlow, Thread twoFlow){
        final long before = System.currentTimeMillis();
        oneFlow.run();
        twoFlow.run();
        final long after = System.currentTimeMillis();

        return (int) (after-before);
    }

    private static void getText(String inputFile, String outputFile) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile)); BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile))) {
            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                bufferedWriter.write(strLine + "\n");
            }
        } catch (IOException e) {
            e.getMessage();
        }
        System.out.println("Текст скопирован!");
    }

    private static String getPathFile(){
        return scanner.nextLine().trim();
    }
}
